from src.airplane import Airplane


class Airline:
    def __init__(self, airplanes: [Airplane]):
        self.airplanes = airplanes
        self.flights = 0

    def __str__(self):
        return f"Lista samolotów {self.airplanes[0]}"

    def get_available_seats_in_all_airplanes(self):
        # all_seats = 0
        # for airplane in self.airplanes:
        #     all_seats += airplane.get_available_seats()
        # return all_seats
        return sum([p.get_available_seats() for p in self.airplanes])

    def realize_flight(self):
        self.flights += 1


if __name__ == '__main__':
    airplane1 = Airplane('Airbus', 200)
    airplane2 = Airplane('Boeing', 400)
    airplane2.board_passengers(150)
    airplanes = [airplane1, airplane2]
    airline = Airline(airplanes)
    print(airline.get_available_seats_in_all_airplanes())
