from src.airline import Airline


def test_airline_with_one_airplane(airline_with_single_airplane):
    assert len(airline_with_single_airplane.airplanes) == 1


def test_airline_with_two_airplanes(airline_with_two_airplanes):
    assert len(airline_with_two_airplanes.airplanes) == 2
