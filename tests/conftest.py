import pytest

from src.airline import Airline
from src.airplane import Airplane


@pytest.fixture
def airline_with_two_airplanes_and_two_flights(test_airplane_boeing, test_airplane_airbus):
    airline = Airline([test_airplane_boeing, test_airplane_airbus])
    airline.realize_flight()
    airline.realize_flight()
    return airline


@pytest.fixture
def test_airplane_airbus():
    return Airplane('Airbus', 200)


@pytest.fixture
def test_airplane_boeing():
    return Airplane('Boeing', 300)


@pytest.fixture
def airline_with_single_airplane(test_airplane_boeing):
    return Airline([test_airplane_boeing])


@pytest.fixture
def airline_with_two_airplanes(test_airplane_boeing, test_airplane_airbus):
    return Airline([test_airplane_boeing, test_airplane_airbus])
