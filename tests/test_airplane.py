import pytest
from assertpy import assert_that

from src.airplane import *


@pytest.fixture
def test_airplane():
    return Airplane('Airbus', 200)


def test_new_airplane_is_of_type_airplane(test_airplane):
    assert_that(test_airplane).is_type_of(test_airplane)


def test_new_airplane_string(test_airplane):
    input_airplane_str = 'Samolot nazwa: Airbus, ilość miejsc: 200, zajęte miejsca: 0, przebieg: 0'
    assert_that(str(test_airplane)).is_equal_to(input_airplane_str)


def test_new_airplane_mileage_is_0_and_seats_taken_is_0(test_airplane):
    assert_that(test_airplane.get_mileage()).is_equal_to(0) and assert_that(test_airplane.seats_taken).is_equal_to(0)


def test_fly_5000_km(test_airplane):
    test_airplane.fly(5000)
    assert_that(test_airplane.get_mileage()).is_equal_to(5000)


def test_fly_5000_and_3000_km(test_airplane):
    test_airplane.fly(5000)
    test_airplane.fly(3000)
    assert_that(test_airplane.get_mileage()).is_equal_to(8000)


def test_if_9999km_does_not_need_service(test_airplane):
    test_airplane.fly(9999)
    assert_that(test_airplane.is_service_required()).is_equal_to(False)


def test_if_10001km_needs_service(test_airplane):
    test_airplane.fly(10001)
    assert_that(test_airplane.is_service_required()).is_equal_to(True)


def test_if_200_seats_airplane_after_180_has_20_seats_available(test_airplane):
    test_airplane.board_passengers(180)
    assert_that(test_airplane.get_available_seats()).is_equal_to(20)


def test_if_200_seats_airplane_after_201_has_0_seats_available(test_airplane):
    test_airplane.board_passengers(201)
    assert_that(test_airplane.get_available_seats()).is_equal_to(0) and assert_that(
        test_airplane.seats_taken).is_equal_to(200)
